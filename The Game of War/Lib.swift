//
//  Lib.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/29/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

import Foundation

public class Lib {
    //**************************************
    //* Math
    //**************************************
    class func random(max:Int) -> Int {
        return Int(arc4random_uniform(UInt32(max+1)));
    }
    
    //**************************************
    //* IO
    //**************************************
    class func prompt() -> String {
        return prompt("");
    }
    
    class func prompt(messagePrompt:String) -> String {
        print(messagePrompt);
        var keyboard = NSFileHandle.fileHandleWithStandardInput()
        var inputData = keyboard.availableData
        return NSString(data: inputData, encoding:NSUTF8StringEncoding)
    }
    
    //**************************************
    //* Exception Handling
    //**************************************
    class func todo() {
        assert(false, "TODO!");
    }
    
}