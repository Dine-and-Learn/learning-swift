//
//  Card.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/29/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

//  Note: the Printable protocol enables objects to be printed directly, as if they were a literal.
public protocol ICard: Printable {
    var suit: Suit { get }
    var value: CardValue { get }
    var description:String { get }
}

public enum Suit : String {
    case Hearts = "Hearts"
    case Spades = "Spades"
    case Diamonds = "Diamonds"
    case Clubs = "Clubs"
}

public enum CardValue: Int, Printable {
    case Ace = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Jack, Queen, King
    public var description:String {
    switch self {
    case .Ace: return "Ace"
    case .Two: return "Two"
    case .Three: return "Three"
    case .Four: return "Four"
    case .Five: return "Five"
    case .Six: return "Six"
    case .Seven: return "Seven"
    case .Eight: return "Eight"
    case .Nine: return "Nine"
    case .Jack: return "Jack"
    case .Queen: return "Queen"
    case .King: return "King"
        }
    }
}