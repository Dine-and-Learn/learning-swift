Lab 2. The Game of War:

Rules:
The Game of War is a card game played between two players and a deck of regular playing cards (no jokers). Here's how the game is played:
1.) The deck is shuffled
2.) Cards are repeatedly drawn from the deck and given to each player's hand, alternating between players, until there are no cards left in the deck. 
3.) The order of the cards in the hand can not be changed from how it was distributed by the deck.
4.) Each round, a player draws one card from the top of their hand and plays it. The player who has the highest card wins the round, and both players' cards are added to the bottom of winner's hand.
5.) You're free to determine which cards are considered "higher" than others, but the default should be:
2 < 3 < 4 < 5 < 6 < 7 < 8 < 9 < Jack < Queen < King < Ace
6.) If there's a tie, 3 additional cards are drawn from the top of each player's hand, and an additional round is played. The winner of that round keeps all current cards currently in play for the initial round that began the "war"
7.) The game is lost by whoever runs out of cards in their hand first unless their last card is used to determine the results of the round.

Tips:
1.) Use the initial structure provided by me to help solve the lab. 
2.) There's a "Library" class that may provide some useful functionality. During the lab, I'll probably update
3.) Google and ask questions


