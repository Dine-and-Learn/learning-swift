//
//  Deck.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/29/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

public protocol IDeck {
    func shuffle()
    func draw() -> ICard
}