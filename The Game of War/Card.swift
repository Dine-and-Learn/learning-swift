//
//  Card.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/30/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

import Foundation

public class Card : ICard {

    //**************************************
    //* State
    //**************************************
    private let _suit:Suit;
    public var suit: Suit {
    get{
        return Suit.Clubs;
    }
    }
    
    private let _value: CardValue;
    public var value: CardValue {
    get {
        return _value;
    }
    }
    
    public var description: String {
        return "\(value) of \(suit.toRaw())";
    }
    
    //**************************************
    //* Identity
    //**************************************
    init(suit:Suit, value:CardValue) {
        _suit = suit;
        _value = value;
    }
    
    //**************************************
    //* Behavior
    //**************************************
}