//
//  Player.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/30/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

import Foundation

public class Player : IPlayer {
    
    //**************************************
    //* State
    //**************************************
    public var name: String {
    get {
        Lib.todo();
        return "TODO!";
    }
    set {
        self.name = newValue;
    }
    }
    
    public var hand: IHand {
    get {
        Lib.todo();
        return Hand();
    }
    set(newHand) {
        self.hand = newHand;
    }
    }
    
    //**************************************
    //* Identity
    //**************************************
    init() {
        Lib.todo();
    }
    
    //**************************************
    //* Behavior
    //**************************************
}