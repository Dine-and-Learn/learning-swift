//
//  Deck.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/30/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

import Foundation

public class Deck : IDeck {
    
    //**************************************
    //* State
    //**************************************
    
    //**************************************
    //* Identity
    //**************************************
    
    //**************************************
    //* Behavior
    //**************************************
    public func shuffle() {
        Lib.todo();
    }
    
    public func draw() -> ICard {
        Lib.todo();
        return Card(suit: Suit.Clubs, value:CardValue.Ace);
    }
}