//
//  Player.swift
//  The Game of War
//
//  Created by Spencer Scorcelletti on 7/29/14.
//  Copyright (c) 2014 Spencer Scorcelletti. All rights reserved.
//

public protocol IPlayer {
    var name: String { get set }
    var hand: IHand { get set }
}